package uz.infinityandro.worldnews.coin

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.GetDatabaseViewModel
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.RickViewModelImpl
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.SaveDataViewModel


val viewModelModul = module {
    viewModel { RickViewModelImpl(repository = get()) }
    viewModel { SaveDataViewModel(context = get()) }
    viewModel { GetDatabaseViewModel(context = get()) }
}