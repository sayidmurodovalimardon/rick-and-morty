package uz.infinityandro.worldnews.coin

import org.koin.dsl.module
import uz.infinityandro.rickandmorty.ui.repository.RickRepository
import uz.infinityandro.rickandmorty.ui.repository.impl.RickRepositoryImpl


val repositoryModule= module {

factory <RickRepository> { RickRepositoryImpl() }

}