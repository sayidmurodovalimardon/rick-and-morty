package uz.infinityandro.rickandmorty.ui.viewmodel.impl

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uz.infinityandro.rickandmorty.data.ResultsItem
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.presenter.room.UserDatabase

@SuppressLint("StaticFieldLeak")
class SaveDataViewModel(private var context: Context) : ViewModel() {

    private var database = UserDatabase.getInstance(context)
    fun saveUser(list: ResultsItem) {
        val user = User(
            0,
            list.image,
            list.gender,
            list.species,
            list.created,
            list.created,
            list.name,
            list.location?.name,
            list.id,
            list.type,
            list.url,
            list.status
        )
        try {
            CoroutineScope(Dispatchers.IO).launch {
                database.userDao().insert(user)
            }

        } catch (e: Exception) {

        }

    }
}