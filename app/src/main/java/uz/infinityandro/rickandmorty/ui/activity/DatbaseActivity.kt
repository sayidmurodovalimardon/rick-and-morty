package uz.infinityandro.rickandmorty.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.rickandmorty.R
import uz.infinityandro.rickandmorty.data.ResultsItem
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.databinding.ActivityDatbaseBinding
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.SaveDataViewModel

class DatbaseActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDatbaseBinding
    lateinit var result:Any
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityDatbaseBinding.inflate(layoutInflater)
        setContentView(binding.root)
        result = intent.getSerializableExtra("rick") as User
        setItems(result as User)
    }
    private fun listeners(result: User) {
        binding.imageBack.setOnClickListener {
            onBackPressed()
        }

        binding.episode.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse(result.url))
            startActivity(intent)
        }
    }

    @SuppressLint("CheckResult")
    private fun setItems(result: User) {
        Glide.with(applicationContext).load(result.image).centerCrop()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return true
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progressDetail.visibility = View.GONE
                    return false
                }

            }).into(binding.sliderViewPager)
        Glide.with(applicationContext).load(result.image).into(binding.movieImage)
        binding.name.text = result.name
        binding.country.text = "${result.status}-${result.species}"
        binding.status.text = "location:${result.location}"
        binding.startDate.text = "created:${result.created}"
        listeners(result)
    }




}