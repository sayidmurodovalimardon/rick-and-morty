package uz.infinityandro.rickandmorty.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.rickandmorty.R
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.databinding.SavedScreenBinding
import uz.infinityandro.rickandmorty.presenter.adapter.RickDatabseRcyclerAdapter
import uz.infinityandro.rickandmorty.ui.activity.DatbaseActivity
import uz.infinityandro.rickandmorty.ui.activity.InfoActivity
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.GetDatabaseViewModel
import uz.infinityandro.worldnews.utils.showToast

class SavedFragment:Fragment(R.layout.saved_screen) {
    private val binding by viewBinding(SavedScreenBinding::bind)
    private val viewMOdel:GetDatabaseViewModel by viewModel()
    var list=ArrayList<User>()
    private lateinit var adapter:RickDatabseRcyclerAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setItems()
        viewModelGet()
        observers()

    }

    private fun observers() {
        viewMOdel.getAllInfoData()
    }

    private fun viewModelGet() {
        viewMOdel.data.observe(requireActivity(),{user->
            if (!user.isNullOrEmpty()){
                list.addAll(user)
                binding.progressTop.visibility=View.GONE
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun setItems() {
        binding.tvShowsRecyclerView.setHasFixedSize(true)
        if (list.isNullOrEmpty()){
            binding.progressTop.visibility=View.GONE
        }
        adapter=RickDatabseRcyclerAdapter(list){result->
            Intent(requireContext(), DatbaseActivity::class.java)?.let {
                it.putExtra("rick",result)
                startActivity(it)
            }

        }
        binding.tvShowsRecyclerView.adapter=adapter
    }
}