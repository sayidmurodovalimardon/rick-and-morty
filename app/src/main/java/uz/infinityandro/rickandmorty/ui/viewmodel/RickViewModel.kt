package uz.infinityandro.rickandmorty.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import uz.infinityandro.rickandmorty.data.Info
import uz.infinityandro.rickandmorty.data.ResultsItem
import java.util.concurrent.Flow

interface RickViewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<Info>
    val progressLiveData: LiveData<Boolean>

    fun getAllBooks(page: Int)


}