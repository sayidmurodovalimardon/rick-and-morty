package uz.infinityandro.rickandmorty.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.rickandmorty.R
import uz.infinityandro.rickandmorty.data.ResultsItem
import uz.infinityandro.rickandmorty.databinding.HomeScreenBinding
import uz.infinityandro.rickandmorty.presenter.adapter.RickRecyclerAdapter
import uz.infinityandro.rickandmorty.ui.activity.InfoActivity
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.RickViewModelImpl
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.SaveDataViewModel
import uz.infinityandro.worldnews.utils.displayToast
import uz.infinityandro.worldnews.utils.showToast

class MainFragment:Fragment(R.layout.home_screen) {
    private val binding by viewBinding(HomeScreenBinding::bind)
    private val viewModel: RickViewModelImpl by viewModel()
    private val saveDataModel:SaveDataViewModel by viewModel()
    private lateinit var adapter: RickRecyclerAdapter
    var list = ArrayList<ResultsItem>()
    var currentPage = 1
    var availablePages = 42
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadUi()
        observers()
    }
    private fun loadViewModel() {
        viewModel.getAllBooks(currentPage)
    }

    private fun observers() {
        viewModel.connectionLiveData.observe(requireActivity(), {
            if (it) {
                showToast("No Internet Connection")
            } else {

            }
        })
        viewModel.errorMessageLiveData.observe(requireActivity(), {

        })
        viewModel.progressLiveData.observe(requireActivity(), {
            if (it) {
                binding.progressTop.visibility = View.VISIBLE
            } else {
                binding.progressTop.visibility = View.GONE
            }
        })
        viewModel.allDataLiveData.observe(requireActivity(), {
            if (it!=null){
                if (!it.results.isNullOrEmpty()) {
                    var ss=availablePages
                    var oldCount = list.size
                    list.addAll(it.results)
                    adapter.notifyItemRangeInserted(oldCount, list.size)
                }
            }
        })

    }


    private fun loadUi() {
        binding.tvShowsRecyclerView.setHasFixedSize(true)
        adapter = RickRecyclerAdapter(list) {result->
            Intent(requireContext(),InfoActivity::class.java)?.let {
                it.putExtra("rick",result)
                startActivity(it)
            }
        }
        binding.tvShowsRecyclerView.adapter = adapter
        binding.tvShowsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (binding.tvShowsRecyclerView.canScrollVertically(1)) {
                    if (currentPage <= availablePages) {
                        currentPage++
                        loadViewModel()
                        binding.progressTop.visibility=View.VISIBLE

                    }
                }
            }
        })
        loadViewModel()
    }

}