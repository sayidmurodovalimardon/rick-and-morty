package uz.infinityandro.rickandmorty.ui.repository.impl

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.rickandmorty.data.Info
import uz.infinityandro.rickandmorty.presenter.api.ApiClient
import uz.infinityandro.rickandmorty.presenter.api.ApiService
import uz.infinityandro.rickandmorty.ui.repository.RickRepository

class RickRepositoryImpl : RickRepository {
    override fun getAllData(page: Int): Flow<Result<Info>> = flow {
        val api = ApiClient.getNews().create(ApiService::class.java)
        val response = api.getAllDataRick(page)
        if (response.isSuccessful) {
            emit(Result.success(response.body()!!))
        } else {
            emit(Result.failure(Throwable(response.errorBody().toString())))
        }
    }.flowOn(Dispatchers.IO)


}