package uz.infinityandro.rickandmorty.ui.viewmodel.impl

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.presenter.room.UserDatabase

class GetDatabaseViewModel(private val context: Context) : ViewModel() {

   private val databse = UserDatabase.getInstance(context)
    var data = MutableLiveData<List<User>>()

    fun getAllInfoData() {
        CoroutineScope(Dispatchers.IO).launch {
            val user = databse.userDao().getNews()
            data.postValue(user)
        }
    }
}