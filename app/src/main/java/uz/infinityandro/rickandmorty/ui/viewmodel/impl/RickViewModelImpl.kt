package uz.infinityandro.rickandmorty.ui.viewmodel.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.rickandmorty.data.Info
import uz.infinityandro.rickandmorty.data.ResultsItem
import uz.infinityandro.rickandmorty.ui.repository.RickRepository
import uz.infinityandro.rickandmorty.ui.repository.impl.RickRepositoryImpl
import uz.infinityandro.rickandmorty.ui.viewmodel.RickViewModel
import uz.infinityandro.worldnews.utils.isConnected

class RickViewModelImpl(private val repository: RickRepository): ViewModel(),RickViewModel {
    override val errorMessageLiveData= MutableLiveData<String>()
    override val connectionLiveData= MutableLiveData<Boolean>()
    override val allDataLiveData= MutableLiveData<Info>()
    override val progressLiveData= MutableLiveData<Boolean>()

    override fun getAllBooks(page: Int) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
          repository.getAllData(page).onEach {
            progressLiveData.postValue(true)
            it.onSuccess {info->
                progressLiveData.postValue(false)
                allDataLiveData.postValue(info)

            }.onFailure {error->
                errorMessageLiveData.postValue(error.message)
            }
        }.launchIn(viewModelScope)


    }


}