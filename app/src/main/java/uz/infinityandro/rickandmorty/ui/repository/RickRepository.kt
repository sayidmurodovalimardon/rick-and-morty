package uz.infinityandro.rickandmorty.ui.repository

import androidx.paging.PagingSource
import kotlinx.coroutines.flow.Flow
import uz.infinityandro.rickandmorty.data.Info
import uz.infinityandro.rickandmorty.data.ResultsItem

interface RickRepository {
    fun getAllData(page: Int): Flow<Result<Info>>
}