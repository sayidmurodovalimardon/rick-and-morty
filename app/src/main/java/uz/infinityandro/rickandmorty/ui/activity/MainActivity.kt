package uz.infinityandro.rickandmorty.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import nl.joery.animatedbottombar.AnimatedBottomBar
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.rickandmorty.R
import uz.infinityandro.rickandmorty.data.ResultsItem
import uz.infinityandro.rickandmorty.databinding.ActivityMainBinding
import uz.infinityandro.rickandmorty.presenter.adapter.RickRecyclerAdapter
import uz.infinityandro.rickandmorty.ui.fragment.MainFragment
import uz.infinityandro.rickandmorty.ui.fragment.SavedFragment
import uz.infinityandro.rickandmorty.ui.viewmodel.impl.RickViewModelImpl
import uz.infinityandro.worldnews.utils.displayToast

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var instance=true
    private lateinit var menuBottom: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (instance){

            binding.animatedBottomBar.selectTabById(R.id.homeFragment,true)
            var transaction=supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentContainerView,MainFragment()).commit()
        }

        selectedItem()
    }
    private fun selectedItem() {
        var fragment: Fragment?=null
        binding.animatedBottomBar.setOnTabSelectListener(object : AnimatedBottomBar.OnTabSelectListener{
            override fun onTabSelected(
                lastIndex: Int,
                lastTab: AnimatedBottomBar.Tab?,
                newIndex: Int,
                newTab: AnimatedBottomBar.Tab
            ) {

                when(newTab.id){
                    R.id.homeFragment->{fragment=MainFragment()}
                    R.id.profileFragment->{fragment=SavedFragment()}
                    else->displayToast("Invalid")
                }
                if (fragment!=null){
                    instance=false
                    var transaction=supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.fragmentContainerView, fragment!!).commit()
                }

            }

        })
    }

}