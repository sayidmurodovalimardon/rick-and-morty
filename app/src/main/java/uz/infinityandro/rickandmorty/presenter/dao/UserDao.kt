package uz.infinityandro.rickandmorty.presenter.dao

import androidx.room.*
import uz.infinityandro.rickandmorty.data.User
@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: User)

    @Query("select * from user")
    suspend fun getNews():List<User>

    @Delete
    suspend fun delete(list: List<User>)
}