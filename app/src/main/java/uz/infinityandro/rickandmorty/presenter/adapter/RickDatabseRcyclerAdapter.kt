package uz.infinityandro.rickandmorty.presenter.adapter

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.rickandmorty.R
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.databinding.ItemContainerRickBinding

class RickDatabseRcyclerAdapter(var list: ArrayList<User>, var listener: (model: User) -> Unit):RecyclerView.Adapter<RickDatabseRcyclerAdapter.VH>() {
    inner class VH(var binding: ItemContainerRickBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(resultsItem: User) {

            binding.root.setOnClickListener {
                listener(resultsItem)
            }

            Glide.with(binding.root.context).load(resultsItem.image).centerCrop()
                .listener(@SuppressLint(
                )
                object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.progress.visibility = View.GONE
                        return false
                    }

                }).into(binding.imageShows)
            if (resultsItem.status.equals("Dead")) {
                binding.aboutLive.setCardBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.error
                    )
                )
            }
            if (resultsItem.status.equals("unknown")) {
                binding.aboutLive.setCardBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.secondary_text
                    )
                )
            }
            if (resultsItem.status.equals("Alive")) {
                binding.aboutLive.setCardBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.yashil
                    )
                )
            }

            binding.textName.text = resultsItem.name
            binding.textNetwork.text="${resultsItem.status}-${resultsItem.species}"
//            val format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZZZZZ")
//            val date = LocalDate.parse(resultsItem.created, format)
            binding.textStarted.text="created:${resultsItem.created}"
            binding.textStatus.text="location:${resultsItem.location}"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            ItemContainerRickBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

}