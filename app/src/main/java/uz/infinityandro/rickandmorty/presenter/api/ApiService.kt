package uz.infinityandro.rickandmorty.presenter.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.infinityandro.rickandmorty.data.Info

interface ApiService {
    @GET("character")
    suspend fun getAllDataRick(
        @Query("page") page:Int
    ):Response<Info>
}