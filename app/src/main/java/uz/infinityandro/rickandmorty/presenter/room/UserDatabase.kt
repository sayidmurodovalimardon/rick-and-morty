package uz.infinityandro.rickandmorty.presenter.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.infinityandro.rickandmorty.data.User
import uz.infinityandro.rickandmorty.presenter.dao.UserDao

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class UserDatabase :RoomDatabase(){
    abstract fun userDao():UserDao

    companion object{

        private var instance:UserDatabase?=null

        fun getInstance(context: Context):UserDatabase{
            synchronized(this){
                instance=DatabaseSave(context)
            }
            return instance!!
        }

        private fun DatabaseSave(context: Context): UserDatabase? =
            Room.databaseBuilder(context.applicationContext,UserDatabase::class.java,"my_rick")
                .allowMainThreadQueries()
                .build()
    }
}