package uz.infinityandro.rickandmorty.app

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import uz.infinityandro.worldnews.coin.repositoryModule
import uz.infinityandro.worldnews.coin.viewModelModul

class App: Application() {
    companion object{
        lateinit var instance:App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(repositoryModule, viewModelModul))
        }
        instance=this
    }
}